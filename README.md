# UNE Slides

A repository containing UNE slides with fixed markdown parser and fonts.

## Usage
Either use `git` to clone the repository, or download it as a `.zip` then extract and open `index.html` in your web browser.

Make sure to check if your [cache is cleared](https://support.planwithvoyant.com/hc/en-us/articles/360046611171-How-to-do-hard-refresh-in-Chrome-Firefox-Safari-and-Microsoft-Edge) if there are any issues.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
Slides written by Will Billingsley.

This work is licensed under a [Creative Commons Attribution 3.0 Australia License](https://creativecommons.org/licenses/by/3.0/au/).